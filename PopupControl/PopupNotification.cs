﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace PopupControl
{
    /// <summary>
    /// Всплывающее сообщение
    /// </summary>
    public class PopupNotification : Control
    {

        static PopupNotification()
        {
            //Регистрируем компонент. Связывает компонент с его стандартным шабоном из файла Generic.xaml
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PopupNotification),
                new FrameworkPropertyMetadata(typeof(PopupNotification)));
        }
        
        public PopupNotification()
        {
            DataContext = this;
            Opacity = DesignerProperties.GetIsInDesignMode(this) ? 1 : 0; //по умолчанию отображается в дизайн-тайме, но скрыт в реал-тайме
        }

        /// <summary>
        /// Отображает всплывающее сообщение
        /// </summary>
        public void Show()
        {
            if (Opacity != 0) return;

            var anim = new DoubleAnimation(0, 1, AppearTime); // плавное изменение значения от 0 до 1 за время AppearTime
            anim.Completed += (sender, args) => RaiseEvent(new RoutedEventArgs(PopupAppearedEvent)); // по окончанию анимации генерируем событие

            BeginAnimation(OpacityProperty, anim); // запускаем анимацию на свойстве прозрачность
        }

        /// <summary>
        /// Скрывает всплывающее сообщение
        /// </summary>
        public void Hide()
        {
            if (Opacity != 1) return;

            var anim = new DoubleAnimation(1, 0, DisappearTime); // аналогично предыдущему, только от 1 до 0 за DisappearTime
            anim.Completed += (sender, args) => RaiseEvent(new RoutedEventArgs(PopupDisappearedEvent));

            BeginAnimation(OpacityProperty, anim);
        }

        /// <summary>
        /// Отображает всплывающее сообщение на определенный промежуток времени. Затем скрывает его
        /// </summary>
        /// <param name="time">Промежуток времени</param>
        public async void ShowPopup(TimeSpan time)
        {
            if (Opacity != 0) return;

            var showAnim = new DoubleAnimation(0, 1, AppearTime);
            showAnim.Completed += (sender, args) => RaiseEvent(new RoutedEventArgs(PopupAppearedEvent));

            var hideAnim = new DoubleAnimation(1, 0, DisappearTime);
            hideAnim.Completed += (sender, args) => RaiseEvent(new RoutedEventArgs(PopupDisappearedEvent));

            //запускаем анимацию появления
            BeginAnimation(OpacityProperty, showAnim);

            // ждем заданный промежуток + время появления
            await Task.Delay(time + AppearTime);

            // запускаем анимацию исчезновения
            BeginAnimation(OpacityProperty, hideAnim);
        }

        #region Dependency props

        /// <summary>
        /// Время появления всплывающего сообщения
        /// </summary>
        public TimeSpan AppearTime
        {
            get { return (TimeSpan) GetValue(AppearTimeProperty); }
            set { SetValue(AppearTimeProperty, value); }
        }

        public static readonly DependencyProperty AppearTimeProperty =
            DependencyProperty.Register("AppearTime", typeof(TimeSpan), typeof(PopupNotification),
                new PropertyMetadata(new TimeSpan(0, 0, 1)));

        /// <summary>
        /// Время исчезновения всплывающего сообщения
        /// </summary>
        public TimeSpan DisappearTime
        {
            get { return (TimeSpan) GetValue(DisappearTimeProperty); }
            set { SetValue(DisappearTimeProperty, value); }
        }

        public static readonly DependencyProperty DisappearTimeProperty =
            DependencyProperty.Register("DisappearTime", typeof(TimeSpan), typeof(PopupNotification),
                new PropertyMetadata(new TimeSpan(0, 0, 1)));


        /// <summary>
        /// Размер иконки
        /// </summary>
        public Size ImageSize
        {
            get { return (Size) GetValue(ImageSizeProperty); }
            set { SetValue(ImageSizeProperty, value); }
        }
        
        public static readonly DependencyProperty ImageSizeProperty =
            DependencyProperty.Register("ImageSize", typeof(Size), typeof(PopupNotification),
                new PropertyMetadata(new Size(0, 0)));

        /// <summary>
        /// Отображаемый текст
        /// </summary>
        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(PopupNotification), new PropertyMetadata(""));

        /// <summary>
        /// Расположение иконки
        /// </summary>
        public ImageSource ImageSource
        {
            get { return (ImageSource) GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(PopupNotification));


        /// <summary>
        /// Радиус скругления уголв
        /// </summary>
        public double CornerRadius
        {
            get { return (double) GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(double), typeof(PopupNotification),
                new PropertyMetadata(0.0D));

        #endregion

        #region Routed events

        public static readonly RoutedEvent PopupAppearedEvent = EventManager.RegisterRoutedEvent("PopupAppeared",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PopupNotification));

        /// <summary>
        /// Событие, генерируемое после появления всплывающего сообщения
        /// </summary>
        public event RoutedEventHandler PopupAppeared
        {
            add { AddHandler(PopupAppearedEvent, value); }
            remove { RemoveHandler(PopupAppearedEvent, value); }
        }

        public static readonly RoutedEvent PopupDisappearedEvent = EventManager.RegisterRoutedEvent("PopupDisappeared",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PopupNotification));

        /// <summary>
        /// Событие, генерируемое после исчезновения всплывающего сообщения
        /// </summary>
        public event RoutedEventHandler PopupDisappeared
        {
            add { AddHandler(PopupDisappearedEvent, value); }
            remove { RemoveHandler(PopupDisappearedEvent, value); }
        }

        #endregion
    }
}