﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(btnShow))
                popup.Show();
            else if (sender.Equals(btnHide))
                popup.Hide();
            else
                popup.ShowPopup(new TimeSpan(0, 0, 4));
        }

        private void Popup_OnPopupAppeared(object sender, RoutedEventArgs e)
        {
            lbEvents.Items.Add("Popup appeared");
        }

        private void Popup_OnPopupDisappeared(object sender, RoutedEventArgs e)
        {
            lbEvents.Items.Add("Popup disappeared");
        }
    }
}
