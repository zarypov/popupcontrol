﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using NUnit.Framework;
using PopupControl;

namespace Tests
{
    [TestFixture, Apartment(ApartmentState.STA)]
    class PopupTests
    {
        private PopupNotification popup;

        [SetUp]
        public void SetUp()
        {
            popup = new PopupNotification();
        }

        [Test]
        public void SetAppearTimeTest()
        {
            popup.AppearTime = new TimeSpan(0,0,10);
            Assert.AreEqual(new TimeSpan(0, 0, 10), popup.AppearTime);
        }

        [Test]
        public void SetDisappearTimeTest()
        {
            popup.DisappearTime = new TimeSpan(0, 0, 10);
            Assert.AreEqual(new TimeSpan(0, 0, 10), popup.DisappearTime);
        }

        [Test]
        public void SetTextTest()
        {
            popup.Text = "TEST";
            Assert.AreEqual("TEST", popup.Text);
        }

        [Test]
        public void SetImageSizeTest()
        {
            popup.ImageSize = new Size(100,100);
            Assert.AreEqual(new Size(100, 100), popup.ImageSize);
        }

        [Test]
        public void SetCornerRadiusTest()
        {
            popup.CornerRadius = 5;
            Assert.AreEqual(5.0, popup.CornerRadius);
        }

        [Test]
        public void SetImageTest()
        {
            popup.ImageSource = GetImage("Testing.jpg");
            Assert.AreEqual(200, popup.ImageSource.Height);
            Assert.AreEqual(200, popup.ImageSource.Width);
        }

        [Test]
        public void AppearEventTest()
        {
            Assert.DoesNotThrow(() =>
            {
                popup.PopupAppeared += PopupEventTest;
                popup.PopupAppeared -= PopupEventTest;
            });
        }

        [Test]
        public void DisappearEventTest()
        {
            Assert.DoesNotThrow(() =>
            {
                popup.PopupDisappeared += PopupEventTest;
                popup.PopupDisappeared -= PopupEventTest;
            });
        }

        [Test]
        public void AppearWhenAlreadyShowed()
        {
            popup.Opacity = 1;
            popup.AppearTime = TimeSpan.FromMilliseconds(100);
            popup.Show();
            Task.Delay(200).Wait();

            Assert.AreEqual(1.0, popup.Opacity);
        }

        [Test]
        public void DisappeadWhenAlreadyHidden()
        {
            popup.Opacity = 0;
            popup.DisappearTime = TimeSpan.FromMilliseconds(100);
            popup.Hide();
            Task.Delay(200).Wait();

            Assert.AreEqual(0.0, popup.Opacity);
        }


        private void PopupEventTest(object sender, RoutedEventArgs routedEventArgs)
        {
        }

        private static BitmapImage GetImage(string name)
        {
            return new BitmapImage(new Uri(@"pack://application:,,,/"
             + Assembly.GetExecutingAssembly().GetName().Name
             + ";component/"
             + name, UriKind.Absolute));
        }
    }
}
